package main



type Country struct {

	Code string `json:"alpha2code"`
	CountryName string `json:"name"`
	CountryFlag string `json:"flag"`
	ArrayOfRes []arrayOfRes `json:"results"`
}

type arrayOfRes struct {
	Occurrence
	SpeciesKey int `json:"speciesKey"`
}

type Occurrence struct {
	Key int `json:"key"`
	Kingdom string `json:"kingdom"`
	Phylum string `json:"phylum"`
	Order string `json:"order"`
	Family string `json:"family"`
	Genus string `json:"genus"`
	ScientificName string `json:"scientificName"`
	Year int `json:"year"`
}

type Species struct {
	Key int `json:"key"`
	Kingdom string `json:"kingdom"`
	Phylum string `json:"phylum"`
	ScientificName string `json:"scientificName"`
	CanonicalName string `json:"canonicalName"`
	Year string `json:"bracketYear"`
}

type Diag struct {
	Gbif int
	RestCountries int
	Version string
	Uptime int
}

