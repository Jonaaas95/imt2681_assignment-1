package main


import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)


var urlCountry string = "https://restcountries.eu/rest/v2/alpha/"
var urlSpecies string = "http://api.gbif.org/v1/species/"
var diag Diag
var startTime time.Time


func handler(w http.ResponseWriter, r *http.Request) {
	//Tells that the content type is json
	http.Header.Add(w.Header(), "Content-type", "application/json")
	//Splits the URL typed in
	URLParts := strings.Split(r.URL.Path, "/")
	//Get response from the urlcountry and what the user types in after the 4th slash
	response, err := http.Get(urlCountry + URLParts[4])
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	//Read statuscode to the diag struct.
	diag.RestCountries = response.StatusCode
	var restCountry = &Country{}
	//Read from response.body to struct
	_ = json.NewDecoder(response.Body).Decode(restCountry)
	//URL to get response from. URLParts[4] and [5] is the parameter the user sends. CountryCode and limit
	var urlOccurrences string = "http://api.gbif.org/v1/occurrence/search?country=" + URLParts[4] + "&limit=" + URLParts[5]
	response1, err := http.Get(urlOccurrences)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	//Read statuscode to the diag struct
	diag.Gbif = response1.StatusCode
	//Read from response1.body to struct
	_ = json.NewDecoder(response1.Body).Decode(restCountry)
	//Reads to the website.
	_ = json.NewEncoder(w).Encode(restCountry)
}

func DiagHandler (w http.ResponseWriter, r *http.Request) {
	//Checks the uptime in seconds.
	diag.Uptime = int(time.Since(startTime))/1000000000
	//Set diag version
	diag.Version = "v1"
	//Read to website.
	_ = json.NewEncoder(w).Encode(diag)
}

func SpeciesHandler(w http.ResponseWriter, r *http.Request) {
	//Tells that the content type is json
	http.Header.Add(w.Header(), "Content-type", "application/json")
	//Splits the URL typed in
	URLParts := strings.Split(r.URL.Path, "/")
	//Get response from the urlSpecies and what the user types in after the 4th slash
	response, err := http.Get(urlSpecies + URLParts[4])
	if err !=nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	//Read statuscode to the diag struct.
	diag.Gbif = response.StatusCode
	var restSpecies = &Species{}
	//Read from response.body to struct
	_ = json.NewDecoder(response.Body).Decode(restSpecies)
	response1, err := http.Get(urlSpecies + URLParts[4] + "/name")
	if err !=nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}
	//Read frmo response1.body to struct
	_ = json.NewDecoder(response1.Body).Decode(restSpecies)
	//Read to website
	_ = json.NewEncoder(w).Encode(restSpecies)
}

func handleRequests () {
	//Tells what function to handle different paths
	http.HandleFunc("/conservation/v1/country/", handler)
	http.HandleFunc("/conservation/v1/species/", SpeciesHandler)
	http.HandleFunc("/conservation/v1/diag/", DiagHandler)
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("PORT must be specified")
	}
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func main() {
	//Starts time when program is running. This is so we can get uptime.
	startTime = time.Now()
	handleRequests()
}